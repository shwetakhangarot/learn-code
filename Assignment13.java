Class Employee
{
    String name;
    int age;
    float salary;

public:
    string getName();
    void setName(string name);
    int getAge();
    void setAge(int age);
    float getSalary();
    void setSalary(float salary);
};
Employee employee;

Is 'employee' an object or a data structure ? Why ?
'employee' is an object as the class Employee consists of both data and behaviour.
2. Look at the below classes and the client code given below on how the object are used and methods invoked. Is there a better way to write the Customer class ? 

public class Customer
{
private
    String firstName;
private
    String lastName;
private  
    Wallet myWallet;
public
    String getFirstName() 
    {
         return firstName; 
    }
public
    String getLastName() 
    { 
        return lastName; 
    }
public
    Wallet getPayment() 
    { 
        return myWallet.value; 
    }
} 
public class Wallet
{
private
    float value;
public
    float getTotalMoney() 
    { 
        return value; 
    }
public
    void setTotalMoney(float newValue) 
    { 
        value = newValue; 
    }
public
    void addMoney(float deposit) 
    { 
        value += deposit; 
    }
public
    void subtractMoney(float debit) 
    { 
        value -= debit; 
    }
} 
Client code….assuming some delivery boy wants to get his payment 
// code from some method inside the delivery boy class... 
payment = 2.00; 
// “I want my two dollars!” 
Wallet theWallet = myCustomer.getPayment(); 
if (theWallet.getTotalMoney() > payment) 
{ 
    theWallet.subtractMoney(payment); 
} 
else { // come back later and get my money }


//Customer class
// Instead of using wallet class we can use payment class to make payment. 
public class Customer
{
private
    String firstName;
private
    String lastName;
private
    Payment payment;
public
    String getFirstName() 
    {
         return firstName; 
    }
public
    String getLastName() 
    { 
        return lastName; 
    }
public
    Payment getPayment() 
    { 
        return myWallet.value; 
    }
} 
public class Payment
{
private
    float value;
public
    float getTotalMoney() 
    { 
        return value; 
    }
public
    void setTotalMoney(float newValue) 
    { 
        value = newValue; 
    }
public
    void addMoney(float deposit) 
    { 
        value += deposit; 
    }
public
    void subtractMoney(float debit) 
    { 
        value -= debit; 
    }
} 
Payment thePyament = myCustomer.getPayment(); 
if (thePyament.getTotalMoney() > payment) 
{ 
    theWallet.subtractMoney(payment); 
} 
else { // come back later and get my money }