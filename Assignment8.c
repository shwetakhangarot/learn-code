#define _DEFAULT_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<sys/wait.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include<sys/msg.h> 
void child();
void parent();
int get_child_exit_status(); 
int msgid; 
int commands=0; 
typedef struct command_type 
{ long type;
 //The command to execute 
 char command[32]; 
 //The arguments for the command
 int args[2]; 
} command_t; 
typedef struct result_type
{ long type; 
 int result;
} result_t; 
//WRITE YOUR CODE HERE 
//Create a message queue and return the message queue id 
int create_message_queue() {     
    int key = ftok("progfile", 65);    
    int msgid = msgget(key, 0666 | IPC_CREAT); return msgid; } 
//send a command via message queue
//msgid - ID of the Message queue 
int send_command(int msgid, command_t cmd) {   
    if(msgsnd(msgid,&cmd, sizeof(cmd), 0)==-1)         
        return -1;    
    return 0; } 
//Read the command from the message queue 
//msgid - ID of the Message queue 
command_t *recv_command(int msgid) {    
    command_t command;    
    command_t *commandvalue=(command_t*)malloc(sizeof(command_t));       
    msgrcv(msgid, &command, sizeof(command), 1, 0);   
    *commandvalue=command;    
    return commandvalue;     
} 
//Send the result via message queue 
//msgid - ID of the Message queue 

int send_result(int msgid, result_t result) {  
    if(msgsnd(msgid, &result, sizeof(result), 1)==-1)    
        return -1;     
    return 0; } 
//Read the Result from the message queue 
//msgid - ID of the Message queue 
result_t *recv_result(int msgid) {   
    result_t result;  
    result_t *resultvalue=(result_t*)malloc(sizeof(result_t));  
    msgrcv(msgid, &result, sizeof(result), 1, 0); 
    *resultvalue=result;  
    return resultvalue; } 
//Delete the message queue 
void delete_message_queue(int msgid) {    
    msgctl(msgid, IPC_RMID, NULL); } 

void parent(){ 
    for(int i=1;i<=commands;i++)
    { command_t cmd;    
     result_t *result;     
     scanf("%s",cmd.command);      
     scanf("%d",&cmd.args[0]);  
     scanf("%d",&cmd.args[1]);    
     cmd.type=1;     
     //WRITE CODE HERE TO READ THE COMMAND FROM INPUT    
     send_command(msgid,cmd);       
     usleep(5000); 
     //SEND THE COMMAND via send_command    
     result=recv_result(msgid);     
     //RECIEVE THE RESULT from CHILD     
     printf("CMD=%s, RES = %d\n",cmd.command,result->result); 
    } 
    printf("Child exited with status:%d\n",get_child_exit_status()); 
    delete_message_queue(msgid); 
} 
void child() {
    for(int i=1;i<=commands;i++)
    { command_t *cmd;   
     cmd=recv_command(msgid);   
     //WRITE CODE to RECIEVE THE COMMAND,use recv_command method.   
     result_t result;     
     result.type=1;        
     if(strcmp(cmd->command,"ADD")==0)       
     {             result.result=cmd->args[0]+cmd->args[1];   
     }         
     else if(strcmp(cmd->command,"SUB")==0)    
     {             result.result=cmd->args[0]-cmd->args[1];    
     }        
     else if(strcmp(cmd->command,"MUL")==0)        
     {             result.result=cmd->args[0]*cmd->args[1];        
     } 
     //WRITE CODE to process the command.         
     send_result(msgid,result);    
     //SEND RESULT via send_result    
     usleep(5000); 
    }        
    exit(commands);
} 
//DO NOT MODIFY CODE BELOW
int main(int argc, char* argv[])
{ pid_t cid; 
 msgid = create_message_queue(); 
 scanf("%d",&commands);
 if(msgid <= 0 )
 { printf("Message Queue creation failed\n"); 
 }    
 cid = fork();
 // Parent process 
 if (cid == 0) 
 { child(); 
 }
 else if(cid > 0 ) 
 { parent();
 } 
} 
//Get the exit code of the child.
int get_child_exit_status()
{         int stat;     
 wait(&stat);  
 return WEXITSTATUS(stat); 
} 